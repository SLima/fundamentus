# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst, Join
from w3lib.html import remove_tags

class InstrumentItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    instrument= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()
    )    
    detail_url= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()
    )
    business_name= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()
    )        
    fancy_name= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()
    )
    instrument_type= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()        
    )      
    business_subsector= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()        
    )    
    business_sector= scrapy.Field(
        input_processor= MapCompose(remove_tags, str.strip),
        output_processor= TakeFirst()        
    )     
