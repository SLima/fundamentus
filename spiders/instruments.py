# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.loader import ItemLoader
from fundamentus.items import InstrumentItem
from urllib.parse import urljoin

class InstrumentsSpider(scrapy.Spider):
    name = 'instruments'
    allowed_domains = ['www.fundamentus.com.br']
    start_urls = ['https://www.fundamentus.com.br/detalhes.php?papel=']

    #Response
    def parse(self, response):
        instr = response.xpath("//table[1]//tr")               
        base='https://www.fundamentus.com.br/detalhes.php?papel='
        i = 0
        for row in instr:
            # i = i +1
            # if i>=50:
            #     break
      
            relative_url = row.xpath("./td[1]/a/@href").get()
            detail_url = urljoin(base,relative_url)
            request = Request(detail_url, callback=self.parseDetails,dont_filter=True)
            yield request

    def parseDetails(self,res):

        table1=res.xpath("//table[1]")
        items = []
        for col in (0,2):
            for row in range(1,6):
                it = {table1.xpath("./tr[%s]/td[%s]/span[2]//text()" % (row,col+1)).get():table1.xpath("./tr[%s]/td[%s]/span//text()" % (row,col+2)).get()}
                items.append(it)
        instrument = {'instrument':items}       

        table3 = res.xpath("//table[3]")
        items = []
        for i in range(2,10):
            it = {table3.xpath("./tr[%s]/td[1]/span//text()" % i).get():table3.xpath("./tr[%s]/td[2]/span//text()" % i).get().strip()}
            items.append(it)
        oscilations = {	'oscilations':items }

        items = []
        for col in (2,4):
            for i in range(2,11):
                it = {table3.xpath("./tr[%s]/td[%s]/span[2]//text()" % (i,col+1)).get():table3.xpath("./tr[%s]/td[%s]/span//text()"  % (i,col+2)).get().strip()}
                items.append(it)
        indicators = {'indicators':items}
        # item = {instrument,oscilations,indicators}

        yield {'instrument':instrument,'oscilations':oscilations,'indicators':indicators}








